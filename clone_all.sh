#!/bin/bash

usage()
{
  echo "usage: $(basename $0)  ssh|krb5|https|gitlab"
  exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

if [[ $1 = "krb5" ]]; then
  PREFIX="https://:@gitlab.cern.ch:8443"
elif [[ $1 = "https" ]]; then
  PREFIX="https://gitlab.cern.ch"
elif [[ $1 = "gitlab" ]]; then
  PREFIX="https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch"
else
  PREFIX="ssh://git@gitlab.cern.ch:7999"
fi
shift

if [ $# -lt 1 ]; then
    TAG=master
else
    TAG=$1
fi

git --version

listOfExternals="external/bootstrap
external/catch
external/concurrentqueue
external/czmq
external/datatables
external/docopt
external/fontawesome
external/highcharts
external/jquery
external/json
external/jwrite
external/libcurl
external/libfabric
external/libnuma
external/logrxi
external/mathjs
external/moment
external/patchelf
external/pybind11
external/readerwriterqueue
external/simdjson
external/simplewebserver
external/spdlog
external/yaml-cpp
external/zyre"


listOfPackages="cmake_tdaq
data_transfer_tools
drivers_rcc
elinkconfig
felig-tools
felixbase
felixbus
felixbus-client
felixcore
felixpy
felix-bus-fs
felix-interface
felix-client
felix-client-thread
felix-def
felix-fpga-flash
felix-mapper
felix-star
felix-tag
felix-unit-test
firmware_loader
flxcard
flxcard_py
flx-firmware-tester
ftools
hdlc_coder
latex
netio
netio-next
packetformat
pcie_hotplug
python_env
regmap
tdaq_tools
wuppercodegen"

for dir in $listOfExternals
do
  name=${dir//[\/]/-}
  echo "$dir"
  if [ ! -d "${dir}" ]; then
    git clone ${PREFIX}/atlas-tdaq-felix/${name}.git ${dir}
  else
    ( cd ${dir}; git pull )
  fi
  echo
done

for dir in $listOfPackages
do
  name=${dir//[\/]/-}
  echo "$dir"
  if [ ! -d "${dir}" ]; then
    git clone --branch ${TAG} ${PREFIX}/atlas-tdaq-felix/${name}.git ${dir}
  else
    ( cd ${dir}; git checkout ${TAG}; git pull )
  fi
  echo
done
