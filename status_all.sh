#!/bin/bash
git --version
echo "."
git status
echo
dirs=($(find . -maxdepth 1 -type d \( ! -name .git \) \( ! -name . \) \( ! -name "x86_64-*" \) \( ! -name external \) -print | sort))
for dir in "${dirs[@]}"; do
  echo ${dir}
  ( cd ${dir}; git status )
  echo
done
