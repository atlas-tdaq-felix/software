#
# Package versions for a release
#
set(cmake_tdaq_version              0.3.1)
set(data_transfer_tools_version     4.0.0)
set(distribution_version            0.2.1)
set(drivers_rcc_version             4.0.2)
set(elinkconfig_version             3.0.0)
set(fatcat_version                  0.1.5)
set(felixbase_version               4.0.0)
set(felixbus_version                4.0.0)
set(felixbus-client_version         4.0.0)
set(felixcore_version               4.0.0)
set(felix-config_version)           4.1.0)
set(felix-mon_version               0.1.3)
set(felix-star_version              0.0.1)
set(flxcard_version                 3.2.7)
set(ftools_version                  1.1.2)
set(hdlc_coder_version              0.2.2)
set(netio_version                   0.8.0)
set(packetformat_version            0.2.2)
set(pepo_version                    1.0.5)
set(regmap_version                  4.0.2)
set(tdaq_tools_version              0.0.1)


#
# Unreleased packages
#
# set(felix-star-watchdog             0.0.0)
# set(benchmarks_version              0.0.0)
# set(cmembuff_version                0.0.0)
# set(felixtest_version               0.0.0)
# set(gbt_config_scripts_version      0.0.0)
# set(gbt_data_emulator_version       0.0.0)
# set(gbt_standalone_scripts_version  0.0.0)
# set(gitlab_ci_runner_version        0.0.0)
# set(gitlab_ci_test_version          0.0.0)
# set(latex_version                   0.0.0)
# set(pcie_hotplug_version            0.0.0)
# set(tdaq_for_flx_version            0.0.0)
# set(tests_version                   0.0.0)
# set(wuppercodegen_version           0.8.0)
