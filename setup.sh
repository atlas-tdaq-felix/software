# Usage: source setup.sh
export FI_VERBS_TX_IOV_LIMIT=30
export FI_VERBS_RX_IOV_LIMIT=30

if [[ "$#" -eq 0 || $1 != "-q" ]]; then
  echo "Setting up FELIX (developer)"
fi

BASEDIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

source $BASEDIR/python_env/bin/activate
source $BASEDIR/cmake_tdaq/bin/setup.sh

