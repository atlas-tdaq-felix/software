#!/bin/bash
git --version
echo "."
git tag -d $1
git push --delete origin $1
echo
dirs=($(find . -maxdepth 1 -type d \( ! -name .git \) \( ! -name . \) \( ! -name "x86_64-*" \) \( ! -name external \) -print | sort))
for dir in "${dirs[@]}"; do
  echo ${dir}
  ( cd ${dir}; git tag -d $1; git push --delete origin $1 )
  echo
done
