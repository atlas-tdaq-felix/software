# Development Instructions

**For support contact the developers (in random order):** 
Mark Dönszelmann, Carlo A. Gottardo, Henk Boterenbrood, Markus Joos

## Table of Content

- [Get the Software](#get-the-sofwtare)
- [Setup and Build](#setup-and-build)
- [Requirements](#requirements)
- [Software Repositories](#software-repositories)
- [External Software](#external-software)
- [Dependencies](#dependencies)


## Get the Software

The checked out top-level 'software' directory only contains a README and some scripts. Every package
lives in its own git repository (see [Software Repositories](#software-repositories)). You can clone them all or you can clone individual packages, see dependencies for which
ones you need.

If you do not have the top-level 'software' directory, for instance if you read this on the website/gitlab, then please clone it first:

```
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/software
cd software
```

- To clone all the packages at once use the script:
```
./clone_all.sh ssh
```
which will create subdirectories for all of the packages listed above.
All will be on the master branch. Any package already checked out will be pulled
for changes.

- To switch to another branch use the script:
```
./checkout_all.sh <branch_name>
```
which will switch all subdirectories to branch_name if that branch exists.

- To update the packages to the latest revision you can run the script:
```
./pull_all.sh  
```
- or update an individual package by changing into its directory and pulling from git:
```
cd flxcard  
git pull  
```

- To see the status of all the packages:
```
./status_all.sh  
```
- To commit and push to a specific package:
```
cd flxcard  
git commit -m "Your Message"  
git push  
```

## Setup and Build

Compiling the code required 'cmake', a c/c++ compiler and LCG for external packages.
The current minimum version of gcc/g++ is 8.3, for which we rely on LCG100.

- Before building or using the software run the set-up script present in this repository
```
source <absolute path to the software folder>/setup.sh
```

- If the software is going to be used with **regmap-5.0** firmware (Phase-II development) add the following environment variable.
```
export REGMAP_VERSION=0x0500
```
If the REGMAP_VERSION is not specified it defaults to 0x0400 (in use for Phase-I systems).


- Configure the build:

```
cd software
cmake_config x86_64-centos7-gcc8-opt
```

- Compile:
```
cd x86_64-centos7-gcc8-opt
make -j8
```

- You can also compile a specific package (for instance flxcard) by:
```
cd x86_64-centos7-gcc8-opt/flxcard
make -j8
```

## Requirements

### Required packages for Centos7

the following system packages are required:
```
sudo yum install gcc
sudo yum install mesa-libGL
sudo yum install xkeyboard-config
sudo yum install librdmacm
sudo yum install make
sudo yum install libpng
sudo yum install libSM
sudo yum install libXrender
sudo yum install fontconfig
sudo yum install libuuid libuuid-devel
```

(in a future version of Centos7 these may be installed by default)

### Driver building

if you want to re-build the driver (part of drivers_rcc) you need the folliwng packages:

```
sudo yum install kernel
sudo yum install kernel-devel
```

### LCG

You need access to LCG on cvmfs either by accessing cvmfs from CERN directory OR using our separate LCG for Felix distribution.

#### Use cvmfs directly from CERNs

NOTE: this may be slow dependening on the network you have to CERN. You could in that case use
a local copy of LCG for Felix (see next item).

Check if you can access /cvmfs/sft.cern.ch/lcg
```
ls /cvmfs/sft.cern.ch/lcg
```
If not, you can install install cvmfs:

```
sudo yum install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
sudo yum install cvmfs cvmfs-config-default
sudo cvmfs_config setup
<edit> /etc/cvmfs/default.local
  (inside cern)
    CVMFS_REPOSITORIES=atlas.cern.ch,sft.cern.ch
    CVMFS_HTTP_PROXY="http://ca-proxy.cern.ch:3128"
or
  (outside cern)
    CVMFS_REPOSITORIES=atlas.cern.ch,sft.cern.ch
    CVMFS_HTTP_PROXY=DIRECT
sudo service autofs restart
cvmfs_config probe
```

#### Use a local copy of LCG for Felix from

This option should be used if no direct access to cvmfs at CERN is available or if access
is too slow. The local copy should preferably kept up to date on a regular basis.
Updates will be provided on the site listed below.

```
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/cvmfs/
```
unpack it in <path to local copy of LCG> and setup the variable:
```
export LCG_BASE=<path to local copy of LCG>/cvmfs/sft.cern.ch/lcg
export TDAQ_BASE=<path to local copy of LCG>/cvmfs/atlas.cern.ch/repo/sw/tdaq
```

### lsb_release

You need to make sure the command lsb_release works.
```
lsb_release
```
If not install it or ask for it to be installed:

```
sudo yum install redhat-lsb
```





## Software Repositories

see https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/software/packages.html

## External Software

| Repository          | Description                                                                                                 |
|:--------------------|:------------------------------------------------------------------------------------------------------------|
| [bootstrap]         | Web Styling from http://getbootstrap.com/getting-started/                                                   |
| [catch]             | Testing framework from https://github.com/philsquared/Catch                                                 |
| [concurrentqueue]   | Lock free multi-reader multi-writer queue from https://github.com/cameron314                                |
| [czmq]              | C API to zeromq from https://github.com/zeromq/czmq                                                         |
| [datatables]        | Web DataTables from https://datatables.net/download/packages                                                |
| [docopt]            | CLI interface helper from https://github.com/docopt/docopt.cpp                                              |
| [highcharts]        | Web Charting library from http://www.highcharts.com/download                                                |
| [jinja]             | Jinja2 for Python from http://jinja.pocoo.org/docs/dev/                                                     |
| [jquery]            | Web JavaScript framework from https://code.jquery.com                                                       |
| [json]              | JSON for Modern C++ from https://github.com/nlohmann/json                                                   |
| [libcurl]           | libcurl https://curl.haxx.se/libcurl/                                                                       |
| [libfabric]         | Open Fabrics Interfaces from https://github.com/ofiwg/libfabric                                             |
| [libnuma]           | numactl-devel providing libnuma                                                                             |
| [markupsafe]        | MarkupSafe for Python from https://pypi.python.org/pypi/MarkupSafe                                          |
| [mathjs]            | Web JavaScript math library for units from http://mathjs.org                                                |
| [meld3]             | An HTML/XML templating engine. https://pypi.org/project/meld3/                                              |
| [patchelf]          | Utility to change rpath for the distrbution from http://nixos.org/patchelf.html                             |
| [pyyaml]            | YAML for Python from http://pyyaml.org/wiki                                                                 |
| [readerwriterqueue] | Lock free single-reader single-writer queue from https://github.com/cameron314                              |
| [setuptools]        | Easily download, build, install, upgrade, and uninstall Python package https://pypi.org/project/setuptools/ |
| [simplewebserver]   | Simple Web Server for C++ from https://github.com/eidheim/Simple-Web-Server                                 |
| [spdlog]            | Logging library from https://github.com/gabime/spdlog                                                       |
| [sphinx]            | Documentation tool for Python from https://pypi.python.org/pypi/Sphinx                                      |
| [supervisor]        | A Process Control System http://supervisord.org                                                             |
| [zeromq]            | ZeroMQ communications library from https://github.com/zeromq/zeromq4-1                                      |
| [zyre]              | Beacon system built on zeromq from https://github.com/zeromq/zyre                                           |


## To add a new external

To add the external:
* make a directory with the name of the new package
* create README.md with instructions on how to download and install new versions
* git init
* git add README
* create a new external-<name>, with description, in group atlas-tdaq-felix on gitlab.cern.ch
* edit the gitlab project and set "builds" to disabled
* git remote add origin ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/external-libcurl.git
* git commit
* git tag version in <name>-00-00-00 format
* git push origin master
* git push --tags

In software (top-level) module:
* Add to the list in README.md
* add to the clone_all.sh script
* add to dependencies.txt

In cmake_tdaq:
* Add module and version to cmake_tdaq/cmake/modules/FELIX-versions.cmake

In module to use new library:
* add to dependencies.txt
* add to CMakeLists.tx a line with `felix_add_external(<name> $ {<name>_version})` or `felix_add_external(<name> ${<name>_version} ${BINARY_TAG})`


## Dependencies

The following packages have dependencies, TDAQ dependencies are listed in parentheses,
optional dependencies in square brackets, dependencies from the FELIX area are in curly-braces.

- cmembuff: drivers_rcc
- elinkconfig: flxcard regmap (Qt5)
- drivers_rcc: -
- fatcat: (PythonLibs SqLite tbb)
- fel: drivers_rcc flxcard regmap
- felixbase: {packetformat} (Boost)
- felixbus: {zyre czmq} (Boost zmq)
- felixbus-client: (zmq) {zyre czmq}
- felixcore: felixbus felixbase netio FlxCard drivers_rcc (tbb Boost yamlcpp)
- felixpy: (Qt5 PythonLibs tbb)
- felixtest: (Qt5 tbb)
- flxcard: regmap drivers_rcc
- ftools: flxcard regmap (Qt5)
- netio: (tbb zmq) [fabric]
- packetformat: -
- pepo: drivers
- regmap: -

### External (LCG) libraries

Some packages need external libs. All external libs are taken from the LCG area on cvmfs.
Each of them are specified in the CMakeLists.txt files of those packages.

We currently rely on the following libs:

- Boost (C++ library)
- pythonlibs (python libraries)
- Qt5 (Base and GUI Library
- SqLite (Database)
- tbb (Intel Threading Building Blocks)
- yamlcpp (read and write yaml files)
- zeromq (ZeroMQ communications library)

### LCG

- LCG via cvmfs is available on the test-bed (after sysadmin request).
- LCG version 98 should be available in ATCN P1, under /sw/atlas/sw/lcg/releases/LCG_98/.
- LCG tar files for FELIX with only the items needed by FELIX is available
- For the binary distributions of FELIX the needed shared objects are extracted and
included in the distribution automatically.
