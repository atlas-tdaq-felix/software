#!/bin/bash
git --version
dirs=($(find . -maxdepth 1 -type d \( ! -name .git \) \( ! -name . \) \( ! -name "x86_64-*" \) \( ! -name external \) -print | sort))
for dir in "${dirs[@]}"; do
  echo ${dir}
  ( cd ${dir}; git push --all; git push --tags )
  echo
done
echo "."
git push --all
git push --tags
