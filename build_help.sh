#!/bin/bash
TAG=$1
BINARY_TAG=$2

HELP=${TAG}-${BINARY_TAG}-help
mkdir ${HELP}
cd ${HELP}

for name in flx-busy-mon flx-config flx-dma-stat flx-i2c flx-info flx-init flx-irq-test flx-reset; do
  ../flxcard/${name} -h > ${name}.txt
done

for name in fcap fcheck fdaq fdaqm fec feconf fedump felink femu fereverse feto fexoff fexofftx fflash fflashprog fgpolarity fice flpgbtconf flpgbtds24 flpgbtio flpgbti2c fpepo fplayback fscaadc fscadac fscads24 fscagbtxconf fscai2c fscaio fscajfile fscajtag fscaid fttcbusy fttcemu fupload fuptest fxvcserver; do
  ../ftools/${name} -h > ${name}.txt
done

for name in felig-config; do  # felig-start felig-stop felig-status
  ../felig-tools/${name} -h > ${name}.txt
done

for name in felix-buslist; do
  ../felixbus-client/${name} -h > ${name}.txt
done

for name in felixcore; do
  ../felixcore/${name} -h > ${name}.txt
done

for name in felix-config; do
  ../felix-config/${name} -h > ${name}.txt
done

for name in felix-busy-toflx felix-busy-tohost felix-dir2bus felix-display-stats felix-elink2file felix-fid felix-fifo2elink felix-file2host felix-get-config-value felix-get-ip felix-get-mode felix-star felix-test-send-busy felix-test-send-toflx-buffered felix-test-send-unbuf felix-test-swrod felix-test-swrod-buffered felix-test-swrod-unbuf felix-toflx felix-tohost; do
   ../felix-star/${name} --help > ${name}.txt
done

cd ..
tar zcvf ${HELP}.tar.gz ${HELP}
